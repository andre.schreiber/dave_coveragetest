using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LambdaFunctionTest
{
    [TestClass]
    public class FunctionTest
    {
        [TestMethod]
        public void FunctionHandlerConvertsToUppercase()
        {
            // arrange
            string input = "ThisIsATest";
            string expected = "THISISATEST";
            string result;

            //act
            result = LambdaTestFunction.Function.FunctionHandler(input, null);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void FunctionHandlerHandlesNullValue()
        {
            // arrange
            string result;

            //act
            result = LambdaTestFunction.Function.FunctionHandler(null, null);

            // assert
            Assert.IsNull(result);
        }
    }
}
